# Package abms.tools

This package provides hopefully helpful functions when dealing
with the results of agent-based model simulations.

## Functions

- `readReports`: read report files into a list of data.tables
- `calcMode`: calculate the mode of a variable
- `quantile.dt`: calculate the quantiles/percentiles of a
  variable. Returns a data.table so that it can be used with
  data.tables' `.by`
